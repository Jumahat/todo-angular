import {Directive, DoCheck, ElementRef} from '@angular/core';

@Directive({
  selector: '[appAutosize]'
})
export class AutosizeDirective implements DoCheck {
  constructor(private el: ElementRef) {
    if (el.nativeElement.tagName !== 'TEXTAREA') {
      throw new Error('appAutosize: This element is not <textarea>');
    }
  }

  ngDoCheck(): void {
    const input: HTMLTextAreaElement = this.el.nativeElement;
    input.style.height = "1px";
    input.style.height = input.scrollHeight + "px";
  }
}
