import {Injectable} from '@angular/core';
import {Todo} from "../interfaces/todo";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private todos: Todo[] = [{
    id: 11,
    title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
    completed: false,
    date: new Date(2021, 10, 3, 12, 0, 0)
  }, {
    id: 12,
    title: 'Accusantium amet aperiam blanditiis culpa cumque debitis est illum modi nam officiis quidem, quis quisquam, saepe temporibus totam velit voluptatum!',
    completed: true,
    date: new Date(1995, 11, 17, 3, 24, 0)
  }, {
    id: 13,
    title: 'Assumenda eveniet laborum recusandae?',
    completed: false,
    date: new Date(1997, 9, 7, 11, 42, 42)
  }, {
    id: 14,
    title: 'SuchALongWordSuchALongWordSuchALongWordSuchALongWordSuchALongWordSuchALongWord Accusantium amet aperiam blanditiis culpa cumque debitis',
    completed: false,
    date: new Date(1997, 9, 7, 11, 42, 42)
  }];

  private todosSubject: BehaviorSubject<Todo[]> = new BehaviorSubject(this.todos);
  data$: Observable<Todo[]> = this.todosSubject.asObservable();

  constructor() {
  }

  get(): Observable<Todo[]> {
    return this.data$;
  }

  add(title: string): void {
    this.todos.push({
      id: this.generateId(),
      title,
      completed: false,
      date: new Date()
    });
    this.todosSubject.next(this.todos);
  }

  delete(id: number): void {
    this.todos = this.todos.filter(t => t.id !== id);
    this.todosSubject.next(this.todos);
  }

  update(id: number, title: string): void {
    const todo = this.todos.find(todo => todo.id === id);
    if (todo) {
      todo.title = title;
      this.todosSubject.next(this.todos);
    }
  }

  toggle(id: number): void {
    const todo = this.todos.find(todo => todo.id === id);
    if (todo) {
      todo.completed = !todo.completed;
      this.todosSubject.next(this.todos);
    }
  }

  clear(): void {
    this.todos = [];
    this.todosSubject.next(this.todos);
  }

  generateId(): number {
    return this.todos.length > 0 ? Math.max(...this.todos.map(todo => todo.id)) + 1 : 0;
  }
}
