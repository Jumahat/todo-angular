import {Component, OnInit} from '@angular/core';

import {TodoService} from "../services/todo.service";
import {Todo} from "../interfaces/todo";
import {Observable} from "rxjs";

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  todos$!: Observable<Todo[]>;

  constructor(private todoService: TodoService) {
  }

  ngOnInit(): void {
    this.getTodos();
  }

  getTodos(): void {
    this.todos$ = this.todoService.get();
  }

  delete(id: number): void {
    this.todoService.delete(id);
  }

  toggle(id: number): void {
    this.todoService.toggle(id);
  }

  update(id: number, title: string): void {
    this.todoService.update(id, title);
  }

  clear() {
    this.todoService.clear();
  }
}
