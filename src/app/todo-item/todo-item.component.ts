import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {Todo} from "../interfaces/todo";

@Component({
  selector: '[app-todo-item]',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent implements OnInit {
  @Input() todo!: Todo;

  @Output() todoUpdate = new EventEmitter<string>();
  @Output() todoToggle = new EventEmitter();
  @Output() todoDelete = new EventEmitter();

  input: string = '';
  edit: boolean = false;

  constructor() {
  }

  ngOnInit(): void {
  }

  editStart(): void {
    this.input = this.todo.title;
    this.edit = true;
  }

  editSave(): void {
    this.todoUpdate.emit(this.input);
    this.edit = false;
  }

  editCancel(): void {
    this.edit = false;
  }

  delete(): void {
    this.todoDelete.emit();
  }

  toggle(): void {
    this.todoToggle.emit();
  }
}
