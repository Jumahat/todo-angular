import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {TodoInputComponent} from './todo-input/todo-input.component';
import {TodoListComponent} from './todo-list/todo-list.component';
import {FormsModule} from "@angular/forms";
import {TodoItemComponent} from './todo-item/todo-item.component';
import {FocusDirective} from './directives/focus.directive';
import { AutosizeDirective } from './directives/autosize.directive';

@NgModule({
  declarations: [
    AppComponent,
    TodoInputComponent,
    TodoListComponent,
    TodoItemComponent,
    FocusDirective,
    AutosizeDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
