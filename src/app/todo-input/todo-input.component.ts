import {Component, OnInit} from '@angular/core';
import {TodoService} from "../services/todo.service";

@Component({
  selector: 'app-todo-input',
  templateUrl: './todo-input.component.html',
  styleUrls: ['./todo-input.component.scss']
})
export class TodoInputComponent implements OnInit {
  input = ''
  requiredError = false

  constructor(private todoService: TodoService) {
  }

  ngOnInit(): void {
  }

  add(): void {
    if (this.input) {
      this.todoService.add(this.input);
      this.input = '';
    } else {
      this.requiredError = true;
    }
  }
}
